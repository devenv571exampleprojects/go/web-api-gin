package customer

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"web-api-gin/auth"
	"web-api-gin/errorhandler"
)

type Customer struct {
	Id        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName" binding:"required"`
}

var (
	idCounter       = 1
	customers       = make(map[int]Customer)
	writeAccessRole = "WRITE"
	readAccessRole  = "READ"
)

// InitRouting initializes routing for the customer resource
func InitRouting(router *gin.Engine) {
	router.POST("/api/customers", auth.Authenticate, authorizeWrite, postHandler)
	router.PUT("/api/customers/:id", putHandler)
	router.GET("/api/customers/:id", auth.Authenticate, authorizeRead, getHandler)
	router.DELETE("/api/customers/:id", deleteHandler)
}

// postHandler processes requests to create customers
func postHandler(c *gin.Context) {
	customer := Customer{}

	if err := c.ShouldBindJSON(&customer); err != nil {
		c.JSON(http.StatusBadRequest, errorhandler.Message{Status: http.StatusBadRequest, Message: err.Error()})
		return
	}

	idCounter++
	customer.Id = idCounter

	customers[customer.Id] = customer

	c.Header("Location", fmt.Sprintf("/api/customers/%d", customer.Id))
	c.String(http.StatusCreated, "")
}

// putHandler processes update requests to existing customers
func putHandler(c *gin.Context) {
	id, stored := checkIfIdStored(c)
	if !stored {
		return
	}

	customer := Customer{}

	if err := c.ShouldBindJSON(&customer); err != nil {
		c.JSON(http.StatusBadRequest, errorhandler.Message{Status: http.StatusBadRequest, Message: err.Error()})
		return
	}

	customer.Id = id
	customers[customer.Id] = customer

	c.String(http.StatusNoContent, "")
}

// getHandler processes read requests for customers
func getHandler(c *gin.Context) {
	id, stored := checkIfIdStored(c)
	if !stored {
		return
	}

	c.JSON(http.StatusOK, customers[id])
}

// deleteHandler processes delete requests for customers
func deleteHandler(c *gin.Context) {
	id, stored := checkIfIdStored(c)
	if !stored {
		return
	}

	delete(customers, id)

	c.String(http.StatusNoContent, "")
}

// checkIfIdStored makes sure, the requested customer is stored. If not, a 404 NOT FOUND is returned
func checkIfIdStored(c *gin.Context) (int, bool) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		c.JSON(http.StatusNotFound,
			errorhandler.Message{
				Status:  http.StatusNotFound,
				Message: fmt.Sprintf("Customer not stored for id %s.", idParam),
			})
		return 0, false
	}

	if _, found := customers[id]; !found {
		c.JSON(http.StatusNotFound,
			errorhandler.Message{
				Status:  http.StatusNotFound,
				Message: fmt.Sprintf("Customer not stored for id %d.", id),
			})
		return 0, false
	}
	return id, true
}

// authorizeWrite checks if all needed roles for writing customers are granted. If not, a 403 FORBIDDEN is returned
func authorizeWrite(c *gin.Context) {
	claim := c.Keys[auth.ClaimKey].(auth.Claim)
	access := auth.Authorize(claim.RealmAccess[auth.RealmRolesKey], writeAccessRole)

	if !access {
		c.AbortWithStatusJSON(http.StatusForbidden, errorhandler.Message{Status: http.StatusForbidden, Message: "Forbidden"})
		return
	}
}

// authorizeRead checks if all needed roles for reading customers are granted. If not, a 403 FORBIDDEN is returned
func authorizeRead(c *gin.Context) {
	claim := c.Keys[auth.ClaimKey].(auth.Claim)
	access := auth.Authorize(claim.RealmAccess[auth.RealmRolesKey], readAccessRole)

	if !access {
		c.AbortWithStatusJSON(http.StatusForbidden, errorhandler.Message{Status: http.StatusForbidden, Message: "Forbidden"})
		return
	}
}

func init() {
	customers[1] = Customer{Id: 1, FirstName: "Mff", LastName: "fgdfg"}
}
