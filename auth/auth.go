package auth

import (
	"context"
	"github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"web-api-gin/errorhandler"
)

type Config struct {
	AuthUrl string
}

type Claim struct {
	RealmAccess    map[string][]string            `json:"realm_access"`
	ResourceAccess map[string]map[string][]string `json:"resource_access"`
}

var (
	config        Config
	ClaimKey      = "claim"
	RealmRolesKey = "roles"
)

// Authenticate authenticates the user against a given provider
func Authenticate(c *gin.Context) {
	ctx := context.Background()
	provider, err := oidc.NewProvider(ctx, config.AuthUrl)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errorhandler.Message{Status: http.StatusUnauthorized, Message: err.Error()})
		return
	}

	oidcConfig := &oidc.Config{
		SkipClientIDCheck: true,
	}

	rawAccessToken := c.GetHeader("Authorization")
	if rawAccessToken == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errorhandler.Message{Status: http.StatusUnauthorized, Message: "Token missing"})
		return
	}

	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errorhandler.Message{Status: http.StatusUnauthorized, Message: "Wrong format of Authorization header"})
		return
	}

	verifier := provider.Verifier(oidcConfig)
	val, err := verifier.Verify(ctx, parts[1])

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errorhandler.Message{Status: http.StatusUnauthorized, Message: err.Error()})
		return
	}

	// add claim to context for subsequent processing of roles
	var claim Claim
	err = val.Claims(&claim)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, errorhandler.Message{Status: http.StatusUnauthorized, Message: err.Error()})
		return
	}
	c.Set(ClaimKey, claim)
}

// Authorize checks, if all needed roles are granted
func Authorize(grantedRoles []string, neededRoles ...string) bool {
	access := true
	for _, needed := range neededRoles {
		neededRoleGranted := false
		for _, granted := range grantedRoles {
			if needed == granted {
				neededRoleGranted = true
				break
			}
		}

		if !neededRoleGranted {
			access = false
			break
		}
	}
	return access
}

func init() {
	config.AuthUrl = "http://localhost:8081/auth/realms/my-realm"
}
