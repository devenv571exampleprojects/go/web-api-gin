package errorhandler

type Message struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}
