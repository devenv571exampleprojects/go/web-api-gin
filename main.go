package main

import (
	"github.com/gin-gonic/gin"
	"web-api-gin/auth"
	"web-api-gin/customer"
)

func main() {
	router := gin.Default()

	customer.InitRouting(router) // init customer routing
	router.HandleMethodNotAllowed = true
	router.NoMethod(auth.Authenticate) // handler for all methods not defined explicitly

	if err := router.Run(":8082"); err != nil {
		panic("startup failed...")
	}
}
